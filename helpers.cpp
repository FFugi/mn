//
// Created by ffugi on 26.05.19.
//

#include <fstream>
#include "helpers.hpp"

std::vector<Point> getData(const std::string &name) {
    std::ifstream file;
    file.open(name);
    std::string line;
    std::vector<Point> result;
    if (!file.good()) {
        throw std::runtime_error("Could not open the file: " + name);
    }
    while (std::getline(file, line)) {
        auto point = line.find(', ');
        auto xStr = line.substr(0, point);
        auto yStr = line.substr(point + 1, line.size());
        double x = std::stod(xStr);
        double y = std::stod(yStr);
        result.push_back({x, y});
    }
    return result;
}

void save(std::vector<Point> &pts, const std::string &name) {
    std::cout << "Saving: " << name << std::endl;
    std::ofstream file;
    file.open(name);
    for (auto &pt : pts) {
        file << pt.x << ", " << pt.y << std::endl;
    }
    file.close();
    std::cout << name << " saved!" << std::endl << std::endl;
}

std::vector<Point> siftData(std::vector<Point> &data, int step) {
    if (step < 1) {
        throw std::invalid_argument(
            "Given step " +
            std::to_string(step) +
            " must be higher than 0");
    }
    std::vector<Point> result;
    for (std::size_t i = 0; i < data.size(); i += step) {
        result.push_back(data[i]);
    }

    return result;
}

std::vector<Point> siftWithoutRange(std::vector<Point> &data, int step, std::size_t from, std::size_t to) {
    if (step < 1) {
        throw std::invalid_argument(
            "Given step " +
            std::to_string(step) +
            " must be higher than 0");
    }
    if (from > to) {
        throw std::invalid_argument("from must be lower than to");
    }
    if (from >= data.size() || to >= data.size()) {
        throw std::invalid_argument("from or to must be lower than size of data");
    }
    std::vector<Point> result;
    std::size_t i = 0;
    for (; i < from; i += step) {
        result.push_back(data[i]);
    }
    for (; i < to; i += 1) {
        result.push_back(data[i]);
    }
    for (; i < data.size(); i += step) {
        result.push_back(data[i]);
    }
    return result;
}

std::vector<double> getRegularPoints(double p1, double p2, std::size_t n) {
    if (n < 2) {
        throw std::invalid_argument("n must be 2 or greater!");
    }
    if (p2 <= p1) {
        std::swap(p2, p1);
    }

    double step = (p2 - p1) / n;
    double current = p1;

    std::vector<double> result;
    while (current < p2) {
        result.push_back(current);
        current += step;
    }
    result.push_back(p2);
    return result;
}
