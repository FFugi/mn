//
// Created by ffugi on 26.05.19.
//

#ifndef PROFILE_HELPERS_HPP
#define PROFILE_HELPERS_HPP

#include <string>
#include "Interpolation.hpp"

void save(std::vector<Point> &pts, const std::string &name);

std::vector<Point> getData(const std::string &name);

std::vector<Point> siftData(std::vector<Point> &data, int step);

std::vector<Point> siftWithoutRange(std::vector<Point> &data, int step, std::size_t from, std::size_t to);

std::vector<double> getRegularPoints(double p1, double p2, std::size_t n);


#endif //PROFILE_HELPERS_HPP
