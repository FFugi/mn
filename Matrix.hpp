#include <utility>

//
// Created by ffugi on 07.05.19.
//

#ifndef PROFILE_MATRIX_HPP
#define PROFILE_MATRIX_HPP


#include <vector>
#include <stdexcept>
#include <iostream>
#include <cmath>

class Matrix {
public:
    Matrix(std::size_t width, std::size_t height, double init_val = 0);

    Matrix(std::size_t width, std::size_t height, std::vector<double> data);

    double get(std::size_t x, std::size_t y) const;

    Matrix& put(std::size_t x, std::size_t y, double element);

    Matrix& swapRows(
        std::size_t a,
        std::size_t b,
        std::size_t from,
        std::size_t to
        );

    Matrix operator+(const Matrix &other) const;

    Matrix operator-(const Matrix &other) const;

    Matrix operator*(const Matrix &other) const;

    Matrix getDiag() const;

    double getNorm() const;

    void print() const;

    std::size_t height() const;

    std::size_t width() const;

private:
    std::vector<double> _data;
    std::size_t _width;
    std::size_t _height;

    std::size_t getIndex(std::size_t x, std::size_t y) const;
};


#endif //PROFILE_MATRIX_HPP
