//
// Created by ffugi on 26.05.19.
//

#include "Matrix.hpp"
#include "Interpolation.hpp"

Matrix getCoefficients(std::vector <Point> pts) {
    if (pts.size() <= 1) {
        throw std::invalid_argument("Vector should have more than 1 points");
    }
    std::size_t polynomialCount = pts.size() - 1;

    std::vector<double> xDiffs;
    xDiffs.reserve(polynomialCount);

    for (std::size_t i = 0; i < pts.size() - 1; i++) {
        xDiffs.push_back(pts[i + 1].x - pts[i].x);
    }

    std::size_t n = (polynomialCount) * 4;
    Matrix a(n, n, 0);
    Matrix b(1, n, 0);

    std::cout << "Building matrix" << std::endl;
    // S_i(x_i) = f(x_i)
    std::size_t row = 0;
    for (std::size_t i = 0; i < polynomialCount; i++, row++) {
        a.put(i * 4, row, 1);       // a
        b.put(0, row, pts[i].y);    // f(x_i)
    }

    for (std::size_t i = 0; i < polynomialCount; i++, row++) {
        a.put(i * 4, row, 1)                                  // a
            .put(i * 4 + 1, row, xDiffs[i])                 // b
            .put(i * 4 + 2, row, xDiffs[i] * xDiffs[i])     // c
            .put(i * 4 + 3, row, xDiffs[i] * xDiffs[i] * xDiffs[i]);   // d
        b.put(0, row, pts[i + 1].y);                          // f(x_i+1)
    }

    // first derivatives
    for (std::size_t i = 0; i < polynomialCount - 1; i++, row++) {
        a.put(i * 4 + 1, row, 1)                              // b_i
            .put(i * 4 + 2, row, 2 * xDiffs[i])               // c_i
            .put(i * 4 + 3, row, 3 * xDiffs[i] * xDiffs[i])   // d_i
            .put(i * 4 + 5, row, -1);                         // b_i+1
    }

    // second derivatives
    for (std::size_t i = 0; i < polynomialCount - 1; i++, row++) {
        a.put(i * 4 + 2, row, 2)                              // c_i
            .put(i * 4 + 3, row, 6 * xDiffs[i])               // d_i
            .put(i * 4 + 6, row, -2);                         // c_i+1
    }

    // edge second derivatives
    a.put(2, row, 1)
        .put(n - 2, row + 1, 2)
        .put(n - 1, row + 1, 6 * xDiffs[xDiffs.size() - 1]);

    std::cout << "Solving set of equatins" << std::endl;
    return performLU(a, b);
}

std::vector <Point> splines(std::vector <Point> &pts, double step) {
    std::string prefix = "Splines: ";
    std::cout << "Splines interpolation" << std::endl;
    std::cout << prefix + "Calculating coefficients" << std::endl;
    Matrix coefficients = getCoefficients(pts);
    std::cout << prefix + "Coefficients calculated" << std::endl;

    double begin = pts[0].x;
    double fin = pts[pts.size() - 1].x;
    double current_x = begin;
    std::vector<Point> result;
    std::size_t current_interval = 0;

    std::cout << prefix + "interpolating" << std::endl;
    while (current_x <= fin) {
        if (current_x > pts[current_interval + 1].x) {
            current_interval++;
        }
        double a = coefficients.get(0, current_interval * 4);
        double b = coefficients.get(0, current_interval * 4 + 1);
        double c = coefficients.get(0, current_interval * 4 + 2);
        double d = coefficients.get(0, current_interval * 4 + 3);

        double x = current_x - pts[current_interval].x;
        double y = a +
                   x * b +
                   x * x * c +
                   x * x * x * d;
        result.push_back({current_x, y});

        current_x += step;
    }
    std::cout << prefix + "Finished" << std::endl << std::endl;
    return result;
}

std::vector <Point> laGrange(std::vector <Point> &pts, double step) {
    std::string prefix = "LaGrange: ";

    std::cout << "LaGrange interpolation" << std::endl;
    std::size_t size = pts.size();
    std::vector<std::pair<double, double>> denominators;

    for (auto &p : pts) {
        double denominator = 1;
        for (auto &pm : pts) {
            if (pm.x != p.x) {
                denominator *= (p.x - pm.x);
            }
        }
        denominators.emplace_back(p.x, denominator);
    }

    double begin = pts[0].x;
    double fin = pts[pts.size() - 1].x;
    double current_x = begin;
    std::vector<Point> result;

    while (current_x <= fin) {
        double current_y = 0;

        for (std::size_t i = 0; i < denominators.size(); i++) {
            double numerator = 1;

            for (std::size_t j = 0; j < denominators.size(); j++) {
                if (j != i) {
                    numerator *= (current_x - pts[j].x);
                }
            }
            numerator *= pts[i].y;
            current_y += numerator / denominators[i].second;
        }

        result.push_back({current_x, current_y});
        current_x += step;
    }
    std::cout << prefix + "Finished" << std::endl << std::endl;
    return result;
}
