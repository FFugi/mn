#include <iostream>
#include <fstream>
#include <time.h>
#include <limits>
#include <tuple>
#include "Equations.hpp"
#include "Interpolation.hpp"
#include "helpers.hpp"


void interpolateFile(const std::string &filename, const int siftStep) {
    const std::string path = "data/" + filename;
    const std::string suffix = std::to_string(siftStep) + "_" + filename;

    std::vector<Point> pts = getData(path);
    auto pts1 = siftData(pts, siftStep);
    auto pts2 = siftWithoutRange(pts1, 2, 0, 6);

    auto splResult = splines(pts1, 10);
    auto laResult = laGrange(pts1, 10);

    auto splResult2 = splines(pts2, 10);
    auto laResult2 = laGrange(pts2, 10);

    save(splResult, "data/splines_" + suffix);
    save(laResult, "data/lagrange_" + suffix);
    save(splResult2, "data/splines_p_" + suffix);
    save(laResult2, "data/lagrange_p_" + suffix);
    save(pts1, "data/data_" + suffix);
    save(pts2, "data/data_p_" + suffix);
}

int main() {
    interpolateFile("bieg.csv", 10);
    interpolateFile("100.csv", 10);
    interpolateFile("gora.csv", 10);
    interpolateFile("bieg.csv", 10);
    interpolateFile("100.csv", 20);
    interpolateFile("gora.csv", 20);
    interpolateFile("bieg.csv", 60);
    interpolateFile("100.csv", 60);
    interpolateFile("gora.csv", 60);
    return 0;
}

