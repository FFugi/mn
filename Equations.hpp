//
// Created by ffugi on 25.05.19.
//

#ifndef PROFILE_EQUATIONS_HPP
#define PROFILE_EQUATIONS_HPP

#include <tuple>
#include "Matrix.hpp"


std::size_t getPivotIdx(
    const Matrix &m,
    const std::size_t col,
    const std::size_t from,
    const std::size_t to
);

std::tuple<Matrix, Matrix, Matrix> luFact(const Matrix &A);

Matrix performLU(const Matrix &A, const Matrix &b);

Matrix performGaussSeidl(const Matrix &A, const Matrix &b);

Matrix getMatrix(std::size_t n, double a1, double a2, double a3);

Matrix getB(std::size_t n);

Matrix performJacobi(const Matrix &A, const Matrix &b);

#endif //PROFILE_EQUATIONS_HPP
