//
// Created by ffugi on 26.05.19.
//

#include <cstdlib>
#include <tuple>
#include "Matrix.hpp"

std::size_t getPivotIdx(const Matrix &m, const std::size_t col, const std::size_t from, const std::size_t to) {
    double max = std::numeric_limits<double>::min();
    std::size_t idx = 0;

    for (std::size_t i = from; i <= to; i++) {
        if (std::abs(m.get(col, i)) > max) {
            max = std::abs(m.get(col, i));
            idx = i;
        }
    }
    return idx;
}

std::tuple <Matrix, Matrix, Matrix> luFact(const Matrix &A) {
    Matrix l = Matrix(A.width(), A.height(), 1).getDiag();
    Matrix p = Matrix(A.width(), A.height(), 1).getDiag();
    Matrix u = A;

    std::size_t m = A.width();
    for (std::size_t k = 0; k < m - 1; k++) {
        std::size_t pivot = getPivotIdx(u, k, k, m - 1);

        u.swapRows(k, pivot, k, m - 1);
        p.swapRows(k, pivot, 0, m - 1);
        l.swapRows(k, pivot, 0, k == 0? 0: k - 1);

        for (std::size_t j = k + 1; j < m; j++) {
            l.put(k, j, u.get(k, j) / u.get(k, k));
            for (std::size_t i = k; i < m; i++) {
                u.put(i, j, u.get(i, j) - l.get(k, j) * u.get(i, k));
            }
        }
    }

    return std::make_tuple(l, u, p);
}

Matrix performLU(const Matrix &A, const Matrix &b) {
    auto lu = luFact(A);
    auto l = std::get<0>(lu);
    auto u = std::get<1>(lu);
    auto p = std::get<2>(lu);
    std::size_t m = A.height();
    Matrix x(1, m);
    Matrix y(1, m);
    auto bp = p * b;

    // forward
    y.put(0, 0, bp.get(0, 0) / l.get(0, 0));
    for (std::size_t j = 1; j < m; j++) {
        double sum = 0;
        for (std::size_t i = 0; i < j; i++) {
            sum += l.get(i, j) * y.get(0, i);
        }
        y.put(0, j, (bp.get(0, j) - sum));
    }

    // backward
    x.put(0, m - 1, y.get(0, m - 1) / u.get(m - 1, m - 1));
    for (std::size_t tj = 0; tj <= m - 2; tj++) {
        std::size_t j = (m - 2) - tj;
        double sum = 0;
        for (std::size_t i = m - 1; i > j; i--) {
            auto a = u.get(i, j);
            auto b = x.get(0, i);
            sum += u.get(i, j) * x.get(0, i);
        }
        x.put(0, j, (y.get(0, j) - sum) / u.get(j, j));
    }
    return x;
}
