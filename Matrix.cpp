//
// Created by ffugi on 07.05.19.
//

#include "Matrix.hpp"

Matrix::Matrix(std::size_t width, std::size_t height, double init_val) {
    _width = width;
    _height = height;
    _data.reserve(width * height);
    for (std::size_t i = 0; i < width * height; i++) {
        _data.push_back(init_val);
    }
}

Matrix::Matrix(std::size_t width, std::size_t height, std::vector<double> data) {
    _width = width;
    _height = height;
    _data = std::move(data);
}

double Matrix::get(std::size_t x, std::size_t y) const {
    return _data.at(getIndex(x, y));
}

Matrix &Matrix::put(std::size_t x, std::size_t y, double element) {
    _data[getIndex(x, y)] = element;
    return *this;
}

Matrix &Matrix::swapRows(std::size_t a, std::size_t b, std::size_t from, std::size_t to) {
    for (std::size_t i = from; i <= to; i++) {
        double tmp = _data[getIndex(i, a)];
        _data[getIndex(i, a)] = _data[getIndex(i, b)];
        _data[getIndex(i, b)] = tmp;
    }
    return *this;
}

Matrix Matrix::operator+(const Matrix &other) const {
    if (other._width != _width || other._height != _height) {
        throw std::invalid_argument("Wrong dimensions!");
    }
    std::vector<double> data;
    data.reserve(_width * _height);
    for (std::size_t i = 0; i < _width * _height; i++) {
        auto val = _data.at(i) + other._data.at(i);
        data.push_back(val);
    }
    return Matrix(_width, _height, data);
}

Matrix Matrix::operator-(const Matrix &other) const {
    if (other._width != _width || other._height != _height) {
        throw std::invalid_argument("Wrong dimensions!");
    }
    std::vector<double> data;
    data.reserve(_width * _height);
    for (std::size_t i = 0; i < _width * _height; i++) {
        auto val = _data.at(i) - other._data.at(i);
        data.push_back(val);
    }
    return Matrix(_width, _height, data);
}

Matrix Matrix::operator*(const Matrix &other) const {
    if (other._height != _width) {
        throw std::invalid_argument("Wrong dimensions!");
    }
    Matrix result(other._width, _height);

    for (std::size_t x = 0; x < other._width; x++) {
        for (std::size_t y = 0; y < _height; y++) {
            double val = 0;
            for (std::size_t i = 0; i < _width; i++) {
                val += this->get(i, y) * other.get(x, i);
            }
            result.put(x, y, val);
        }
    }
    return result;
}

Matrix Matrix::getDiag() const {
    Matrix result(_width, _height);
    for (std::size_t i = 0; i < _height; i++) {
        result.put(i, i, this->get(i, i));
    }
    return result;
}

std::size_t Matrix::getIndex(std::size_t x, std::size_t y) const {
    return y * _width + x;
}

double Matrix::getNorm() const {
    double sum = 0;
    for (auto item : _data) {
        sum += item * item;
    }
    return std::sqrt(sum);
}

void Matrix::print() const {
    for (std::size_t y = 0; y < _height; y++) {
        for (std::size_t x = 0; x < _width; x++) {
            std::cout << _data.at(getIndex(x, y)) << "\t\t";
        }
        std::cout << std::endl;
    }
    std::cout << "--------------------" << std::endl;
}

std::size_t Matrix::height() const {
    return _height;
}

std::size_t Matrix::width() const {
    return _width;
}
