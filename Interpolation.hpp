//
// Created by ffugi on 25.05.19.
//

#ifndef PROFILE_INTERPOLATION_HPP
#define PROFILE_INTERPOLATION_HPP

#include <vector>
#include <cmath>
#include "Matrix.hpp"
#include "Equations.hpp"

struct Point {
    double x;
    double y;
};

Matrix getCoefficients(std::vector<Point> pts);

std::vector<Point> splines(std::vector<Point> &pts, double step);

std::vector<Point> laGrange(std::vector<Point> &pts, double step);

#endif //PROFILE_INTERPOLATION_HPP
